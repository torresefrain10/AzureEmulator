# Azure Emulator

Welcome to Azure Emulator. Azure Emulator is still in RC (Release Candidate) version, many bugs and glitches can occur. The code is far from being perfect and needs a lot of optimizations. Any and all help is welcome!

Azure Emulator is created by Azure Team:
>Jaden (Moonshine)     
>Enrico (Diesel)    
>Tim (TimNL)    
>Jamal (SirJamal)   
>Kyle (KyleeIsProzZ)     
>Antoine (TyrexFr)     
>Dominic (Dominicus)     
>Cankiee     
>Gerard 

### Version
2.0.100

### Habbo Release

PRODUCTION-201506161211-776084490

### Plugins    

* Filter System
* Words Ban System
* Commands System
* Language System