#region

using System.Collections.Generic;
using System.Linq;

#endregion

namespace Azure.HabboHotel.Misc
{
    /// <summary>
    /// Class AntiMutant.
    /// </summary>
    internal class AntiMutant
    {
        /// <summary>
        /// The _parts
        /// </summary>
        private readonly Dictionary<string, Dictionary<string, Figure>> _parts;

        /// <summary>
        /// Initializes a new instance of the <see cref="AntiMutant"/> class.
        /// </summary>
        public AntiMutant()
        {
            _parts = new Dictionary<string, Dictionary<string, Figure>>();
        }

        /// <summary>
        /// Gets the look gender.
        /// </summary>
        /// <param name="look">The look.</param>
        /// <returns>System.String.</returns>
        private string GetLookGender(string look)
        {
            var figureParts = look.Split('.');

            foreach (string part in figureParts)
            {
                var tPart = part.Split('-');
                if (tPart.Count() < 2)
                    continue;
                var partName = tPart[0];
                var partId = tPart[1];
                if (partName != "hd")
                    continue;
                return _parts.ContainsKey(partName) && _parts[partName].ContainsKey(partId)
                    ? _parts[partName][partId].Gender
                    : "U";
            }
            return "U";
        }
    }
}