#region

using System;
using System.Threading;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using System.Diagnostics;

#endregion

namespace Azure.HabboHotel.Misc
{
    /// <summary>
    /// Class LowPriorityWorker.
    /// </summary>
    internal class LowPriorityWorker
    {
        /// <summary>
        /// The _user peak
        /// </summary>
        private static int _userPeak;

        private static string _lastDate;

        private static bool isExecuted;

        private static Stopwatch consoleTitleWorkerWatch;
        private static Stopwatch lowPriorityProcessWatch;

        /// <summary>
        /// The _m timer
        /// </summary>
        private static Timer _mTimer;

        /// <summary>
        /// Initializes the specified database client.
        /// </summary>
        /// <param name="dbClient">The database client.</param>
        internal static void Init(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT userpeak FROM server_status");
            _userPeak = dbClient.GetInteger();

            lowPriorityProcessWatch = new Stopwatch();
            consoleTitleWorkerWatch = new Stopwatch();
            lowPriorityProcessWatch.Start();
            consoleTitleWorkerWatch.Start();
        }

        /// <summary>
        /// Starts the processing.
        /// </summary>
        internal static void StartProcessing()
        {
            _mTimer = new Timer(Process, null, 0, 60000);
        }

        /// <summary>
        /// Processes the specified caller.
        /// </summary>
        /// <param name="caller">The caller.</param>
        internal static void Process(object caller)
        {
            if (lowPriorityProcessWatch.ElapsedMilliseconds >= 35000 || !isExecuted)
            {
                isExecuted = true;
                lowPriorityProcessWatch.Restart();

                var clientCount = AzureEmulator.GetGame().GetClientManager().ClientCount();
                var loadedRoomsCount = AzureEmulator.GetGame().GetRoomManager().LoadedRoomsCount;
                var dateTime = new DateTime((DateTime.Now - AzureEmulator.ServerStarted).Ticks);

                Console.Title = string.Concat("AzureEmulator v" + AzureEmulator.Version + "." + AzureEmulator.Build + " | TIME: ",
                    int.Parse(dateTime.ToString("dd")) - 1 + dateTime.ToString(":HH:mm:ss"), " | ONLINE COUNT: ",
                    clientCount, " | ROOM COUNT: ", loadedRoomsCount);
                using (var queryReactor = AzureEmulator.GetDatabaseManager().GetQueryReactor())
                {
                    if (clientCount > _userPeak)
                        _userPeak = clientCount;

                    _lastDate = DateTime.Now.ToShortDateString();
                    queryReactor.RunFastQuery(string.Concat("UPDATE server_status SET stamp = '",
                        AzureEmulator.GetUnixTimeStamp(), "', users_online = ", clientCount, ", rooms_loaded = ",
                        loadedRoomsCount, ", server_ver = 'Azure Emulator', userpeak = ", _userPeak));
                }

                AzureEmulator.GetGame().GetNavigator().LoadNewPublicRooms();

            }
        }
    }
}