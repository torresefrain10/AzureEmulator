#region

using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;

#endregion

namespace Azure.HabboHotel.Items.Interactor
{
    internal class InteractorJukebox : IFurniInteractor
    {
        public void OnPlace(GameClient session, RoomItem item)
        {
            item.GetRoom().GetRoomMusicController().Reset();
            item.GetRoom().GetRoomMusicController().LinkRoomOutputItem(item);

            if (item.ExtraData == "1")
            {
                if (item.GetRoom().GetRoomMusicController().PlaylistSize > 0)
                    item.GetRoom().GetRoomMusicController().Start();
                else
                {
                    item.ExtraData = "0";
                    item.UpdateState();
                }
            }
        }

        public void OnRemove(GameClient session, RoomItem item)
        {
            if (item.GetRoom().GetRoomMusicController().IsPlaying)
            {
                item.GetRoom().GetRoomMusicController().Stop();
                item.GetRoom().GetRoomMusicController().BroadcastCurrentSongData(session.GetHabbo().CurrentRoom);
            }

            item.GetRoom().GetRoomMusicController().Reset();
        }

        public void OnTrigger(GameClient session, RoomItem item, int request, bool hasRights)
        {
            if (!hasRights)
                return;

            switch (request)
            {
                case -1: // Open interface

                    break;

                default:
                    if (item.ExtraData == "1")
                    {
                        item.GetRoom().GetRoomMusicController().Stop();
                        item.ExtraData = "0";
                    }
                    else
                    {
                        item.GetRoom().GetRoomMusicController().Start();
                        item.ExtraData = "1";
                    }

                    item.UpdateState();

                    item.GetRoom().GetRoomMusicController().BroadcastCurrentSongData(item.GetRoom());
                    break;
            }
        }

        public void OnUserWalk(GameClient session, RoomItem item, RoomUser user)
        {
        }

        public void OnWiredTrigger(RoomItem item)
        {
            if (item.ExtraData == "1")
            {
                item.GetRoom().GetRoomMusicController().Stop();
                item.ExtraData = "0";
            }
            else
            {
                item.GetRoom().GetRoomMusicController().Start();
                item.ExtraData = "1";
            }
            item.UpdateState();
        }
    }
}