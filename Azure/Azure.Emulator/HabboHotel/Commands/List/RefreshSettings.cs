﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshSettings. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshSettings : Command
    {
        public RefreshSettings()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_settings_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                AzureEmulator.ConfigData = new ConfigData(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_settings_succesfully"));
        }
    }
}