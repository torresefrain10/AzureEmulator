﻿#region

using System.Text;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class UserInfo. This class cannot be inherited.
    /// </summary>
    internal sealed class UserInfo : Command
    {
        public UserInfo()
        {
            MinParams = 1;
            Description = "Get information about a user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var userName = pms[0];
            if (!string.IsNullOrEmpty(userName))
            {
                var clientByUserName = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(userName);
                if (clientByUserName == null)
                {
                    session.SendNotif("Funny is niet online!");
                    return;
                }
                
                var habbo = clientByUserName.GetHabbo();
                var builder = new StringBuilder();
                if (habbo.CurrentRoom != null)
                {
                    builder.AppendFormat(" - ROOM INFORMATION [{0}] - \r", habbo.CurrentRoom.RoomId);
                    builder.AppendFormat("Owner: {0}\r", habbo.CurrentRoom.RoomData.Owner);
                    builder.AppendFormat("Room Name: {0}\r", habbo.CurrentRoom.RoomData.Name);
                    builder.Append(
                        string.Concat("Current Users: ", habbo.CurrentRoom.UserCount, "/", habbo.CurrentRoom.RoomData.UsersMax));
                }
                session.SendNotif(string.Concat("User info for: ", userName, " \rUser ID: ", habbo.Id, ":\rRank: ", habbo.Rank, "\rCurrentTalentLevel: ", habbo.CurrentTalentLevel, " \rCurrent Room: ", habbo.CurrentRoomId, " \rCredits: ", habbo.Credits, "\rDuckets: ", habbo.ActivityPoints, "\rDiamonds: ", habbo.Diamonds, "\rMuted: ", habbo.Muted.ToString(), "\r\r\r", builder.ToString()));
                return;
            }
            session.SendNotif("Voer een Funnynaam in");
        }
    }
}