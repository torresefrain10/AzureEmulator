﻿#region

using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Kick. This class cannot be inherited.
    /// </summary>
    internal sealed class Kick : Command
    {
        public Kick()
        {
            MinParams = -1;
            Description = "Kick user from room.";
            Usage = "[username] [message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var userName = pms[0];
            var userSession = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(userName);
            if (userSession == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (session.GetHabbo().Rank <= userSession.GetHabbo().Rank)
            {
                session.SendNotif(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            if (userSession.GetHabbo().CurrentRoomId < 1)
            {
                session.SendNotif(TextManager.GetText("command_kick_user_not_in_room"));
                return;
            }
            var room = AzureEmulator.GetGame().GetRoomManager().GetRoom(userSession.GetHabbo().CurrentRoomId);
            if (room == null)
            {
                return;
            }

            room.GetRoomUserManager().RemoveUserFromRoom(userSession, true, false);
            userSession.CurrentRoomUserId = -1;
            if (pms.Length > 1)
            {
                userSession.SendNotif(
                    string.Format(TextManager.GetText("command_kick_user_mod_default") + "{0}.",
                        string.Join(" ", pms.Skip(1))));
            }
            else
            {
                userSession.SendNotif(TextManager.GetText("command_kick_user_mod_default"));
            }
        }
    }
}