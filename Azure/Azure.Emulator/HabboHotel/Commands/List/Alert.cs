﻿#region

using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Alert. This class cannot be inherited.
    /// </summary>
    internal sealed class Alert : Command
    {
        public Alert()
        {
            MinParams = -1;
            Description = "Send personal alert to user.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var userName = pms[0];
            var msg = string.Join(" ", pms.Skip(1));

            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(userName);
            if (client == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            client.SendNotif(string.Format("{0} \r\r-{1}", msg, session.GetHabbo().UserName));
        }
    }
}