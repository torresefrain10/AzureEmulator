﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Enable. This class cannot be inherited.
    /// </summary>
    internal sealed class Enable : Command
    {
        public Enable()
        {
            MinParams = 1;
            Description = "Enable item.";
            Usage = "[id]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().VIP || session.GetHabbo().HasFuse("user_is_staff");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user =
            session.GetHabbo()
                .CurrentRoom.GetRoomUserManager()
                .GetRoomUserByHabbo(session.GetHabbo().UserName);
            if (user.RidingHorse) return;
            if (user.IsLyingDown) return;
            ushort effect;

            if (!ushort.TryParse(pms[0], out effect)) return;
            if (effect == 102 && !session.GetHabbo().HasFuse("moderator")) return;
            if (effect == 140 && !(session.GetHabbo().VIP)) return;
            if (effect == 178 && !session.GetHabbo().HasFuse("moderator")) return;

            session.GetHabbo()
                .GetAvatarEffectsInventoryComponent()
                .ActivateCustomEffect(effect);

        }
    }
}