﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.PathFinding;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class PullUser. This class cannot be inherited.
    /// </summary>
    internal sealed class PullUser : Command
    {

        public PullUser()
        {
            MinParams = 1;
            Description = "Pull user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().VIP || session.GetHabbo().HasFuse("user_is_staff");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            var user = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (user == null) return;

            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (client.GetHabbo().Id == session.GetHabbo().Id)
            {
                session.SendWhisper(TextManager.GetText("command_pull_error_own"));
                return;
            }
            var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(client.GetHabbo().Id);
            if (user2 == null) return;
            if (user2.TeleportEnabled)
            {
                session.SendWhisper(TextManager.GetText("command_error_teleport_enable"));
                return;
            }

            if (PathFinder.GetDistance(user.X, user.Y, user2.X, user2.Y) > 2)
            {
                session.SendWhisper(TextManager.GetText("command_pull_error_far_away"));
                return;
            }
            if ((user.RotBody % 2) != 0) user.RotBody--;
            switch (user.RotBody)
            {
                case 0:
                    user2.MoveTo(user.X, user.Y - 1);
                    break;

                case 2:
                    user2.MoveTo(user.X + 1, user.Y);
                    break;

                case 4:
                    user2.MoveTo(user.X, user.Y + 1);
                    break;

                case 6:
                    user2.MoveTo(user.X - 1, user.Y);
                    break;
            }
        }
    }
}