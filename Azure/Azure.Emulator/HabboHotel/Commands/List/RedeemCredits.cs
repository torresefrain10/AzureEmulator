﻿#region

using System;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RedeemCredits.
    /// </summary>
    internal sealed class RedeemCredits : Command
    {
        internal RedeemCredits()
        {   
            Description = "Change invetory items back to credits.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            try
            {
                session.GetHabbo().GetInventoryComponent().Redeemcredits(session);
                session.SendNotif(TextManager.GetText("command_redeem_credits"));
            }
            catch (Exception e)
            {
                Writer.Writer.LogException(e.ToString());
                session.SendNotif(TextManager.GetText("command_redeem_credits"));
            }
        }
    }
}