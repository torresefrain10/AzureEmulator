﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshExtraThings : Command
    {
        public RefreshExtraThings()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_extra_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            AzureEmulator.GetGame().GetHallOfFame().RefreshHallOfFame();
            AzureEmulator.GetGame().GetRoomManager().GetCompetitionManager().RefreshCompetitions();
            AzureEmulator.GetGame().GetTargetedOfferManager().LoadOffer();
            using (var adapter = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                AzureEmulator.GetGame().GetCraftingManager().Initialize(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}