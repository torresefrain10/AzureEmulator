﻿#region

using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms;
using System.Collections.Generic;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class PickAll. This class cannot be inherited.
    /// </summary>
    internal sealed class PickAll : Command
    {
        public PickAll()
        {
            MinParams = 0;
            Description = "Pickup all furni in the current room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("manager") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            Room room = session.GetHabbo().CurrentRoom;
            List<RoomItem> roomItemList = room.GetRoomItemHandler().RemoveAllFurniture(session);
            if (session.GetHabbo().GetInventoryComponent() == null)
            {
                return;
            }

            room.GetRoomItemHandler().RemoveItemsByOwner(ref roomItemList, ref session);
        }
    }
}