﻿#region

using System;
using System.Threading.Tasks;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Shutdown. This class cannot be inherited.
    /// </summary>
    internal sealed class Restart : Command
    {
        public Restart()
        {
            MinParams = 0;
            Description = "Restart Azure Emulator.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            AzureEmulator.GetGame()
            .GetModerationTool()
            .LogStaffEntry(session.GetHabbo().UserName, string.Empty, "Restart",
                "Issued Restart command!");
            new Task(AzureEmulator.PerformRestart).Start();
        }
    }
}