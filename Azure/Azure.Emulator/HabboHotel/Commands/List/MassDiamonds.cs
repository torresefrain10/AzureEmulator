﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassDiamonds. This class cannot be inherited.
    /// </summary>
    internal sealed class MassDiamonds : Command
    {
        public MassDiamonds()
        {
            MinParams = -1;
            Description = "Mass diamonds.";
            Usage = "[diamonds]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_user_can_give_currency");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            int amount;
            if (!int.TryParse(pms[0], out amount))
            {
                session.SendNotif(TextManager.GetText("enter_numbers"));
                return;
            }
            foreach (GameClient client in AzureEmulator.GetGame().GetClientManager().Clients.Values)
            {
                if (client == null || client.GetHabbo() == null) continue;
                var habbo = client.GetHabbo();
                habbo.Diamonds += amount;
                client.GetHabbo().UpdateSeasonalCurrencyBalance();
                client.SendNotif(TextManager.GetText("command_diamonds_one_give") + amount + (TextManager.GetText("command_diamonds_two_give")));
            }
        }
    }
}