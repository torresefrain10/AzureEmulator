﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class GiveCredits. This class cannot be inherited.
    /// </summary>
    internal sealed class GiveCredits : Command
    {
        public GiveCredits()
        {
            MinParams = 2;
            Description = "Give user credits.";
            Usage = "[username] [credits]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_user_can_give_currency");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            int amount;
            if (!int.TryParse(pms[1], out amount))
            {
                session.SendWhisper(TextManager.GetText("enter_numbers"));
                return;
            }
            client.GetHabbo().Credits += amount;
            client.GetHabbo().UpdateCreditsBalance();
            client.SendNotif(string.Format(TextManager.GetText("staff_gives_credits"), session.GetHabbo().UserName, amount));
        }
    }
}