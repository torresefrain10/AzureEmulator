﻿#region

using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Support;
using Azure.Configuration;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class BanUser. This class cannot be inherited.
    /// </summary>
    internal sealed class BanUser : Command
    {
        public BanUser()
        {
            MinParams = -2;
            Description = "Ban user by account.";
            Usage = "[username] [time] [reason]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);

            if (user == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            try
            {
                var length = int.Parse(pms[1]);

                var message = pms.Length < 3 ? string.Empty : string.Join(" ", pms.Skip(2));
                if (string.IsNullOrWhiteSpace(message)) message = TextManager.GetText("command_ban_user_no_reason");

                ModerationTool.BanUser(session, user.GetHabbo().Id, length, message);
                AzureEmulator.GetGame()
                    .GetModerationTool()
                    .LogStaffEntry(session.GetHabbo().UserName, user.GetHabbo().UserName, "Ban",
                        string.Format("USER:{0} TIME:{1} REASON:{2}", pms[0], pms[1], message));
            }
            catch
            {

                // error handle 
            }
        }
    }
}