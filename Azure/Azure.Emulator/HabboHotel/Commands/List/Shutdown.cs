﻿#region

using System;
using System.Threading.Tasks;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Shutdown. This class cannot be inherited.
    /// </summary>
    internal sealed class Shutdown : Command
    {
        public Shutdown()
        {
            MinParams = 0;
            Description = "Shutdown Azure Emulator.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            AzureEmulator.GetGame()
                .GetModerationTool()
                .LogStaffEntry(session.GetHabbo().UserName, string.Empty, "Shutdown",
                    "Issued shutdown command!");
            new Task(AzureEmulator.PerformShutDown).Start();
        }
    }
}