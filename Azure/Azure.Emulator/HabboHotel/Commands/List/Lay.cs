﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Lay. This class cannot be inherited.
    /// </summary>
    internal sealed class Lay : Command
    {
        public Lay()
        {
            MinParams = 0;
            Description = "User lay down.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var currentRoom = session.GetHabbo().CurrentRoom;

            var roomUserByHabbo = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (roomUserByHabbo == null) return;

            if (roomUserByHabbo.IsSitting || roomUserByHabbo.RidingHorse || roomUserByHabbo.IsWalking ||
                roomUserByHabbo.Statusses.ContainsKey("lay"))
                return;

            if (roomUserByHabbo.RotBody % 2 != 0) roomUserByHabbo.RotBody--;
            roomUserByHabbo.Statusses.Add("lay", "0.55");
            roomUserByHabbo.IsLyingDown = true;
            roomUserByHabbo.UpdateNeeded = true;
            return;
        }
    }
}