﻿#region

using System;
using System.Linq;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class HotelAlertLink : Command
    {
        public HotelAlertLink()
        {
            MinParams = -1;
            Description = "Hotel alert with link.";
            Usage = "[link] [message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var messageUrl = pms[0];
            var messageStr = string.Join(" ", pms.Skip(1));

            AzureEmulator.GetGame()
                .GetClientManager()
                .SendSuperNotif("${catalog.alert.external.link.title}", messageStr, "game_promo_small", session,
                    messageUrl, "${facebook.create_link_in_web}", true, false);
        }
    }
}