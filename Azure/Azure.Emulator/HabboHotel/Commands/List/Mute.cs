﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Mute. This class cannot be inherited.
    /// </summary>
    internal sealed class Mute : Command
    {
        public Mute()
        {
            MinParams = 1;
            Description = "Mute user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null || client.GetHabbo() == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (client.GetHabbo().Rank >= 4)
            {
                session.SendNotif(TextManager.GetText("user_is_higher_rank"));
            }
            AzureEmulator.GetGame()
                .GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, client.GetHabbo().UserName,
                    "Mute", "Muted user");
            client.GetHabbo().Mute();
        }
    }
}