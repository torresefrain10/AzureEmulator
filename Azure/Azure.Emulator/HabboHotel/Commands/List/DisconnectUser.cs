#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class DisconnectUser. This class cannot be inherited.
    /// </summary>
    internal sealed class DisconnectUser : Command
    {
        public DisconnectUser()
        {
            MinParams = 1;
            Description = "Disconnect a user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (user == null || user.GetHabbo() == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            try
            {
                user.GetConnection().Dispose();
                AzureEmulator.GetGame()
                    .GetModerationTool()
                    .LogStaffEntry(session.GetHabbo().UserName, user.GetHabbo().UserName, "dc",
                        string.Format("Disconnect User[{0}]", pms[1]));
            }
            catch
            {

            }
        }
    }
}