﻿# region
using System;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
# endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshSongs. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshText : Command
    {
        public RefreshText()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_texts_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (IQueryAdapter client = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                TextManager.ClearText();
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}