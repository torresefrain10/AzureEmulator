﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Override. This class cannot be inherited.
    /// </summary>
    internal sealed class Override : Command
    {
        public Override()
        {
            MinParams = 0;
            Description = "Walk over objects.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("ambassadeur");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var currentRoom = session.GetHabbo().CurrentRoom;

            var roomUserByHabbo = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (roomUserByHabbo == null) return;
            roomUserByHabbo.AllowOverride = !roomUserByHabbo.AllowOverride;
        }
    }
}