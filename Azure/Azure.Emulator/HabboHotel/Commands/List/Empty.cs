﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Empty. This class cannot be inherited.
    /// </summary>
    internal sealed class Empty : Command
    {
        public Empty()
        {
            MinParams = 0;
            Description = "Remove everything from inventory.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            session.GetHabbo().GetInventoryComponent().ClearItems();
        }
    }
}