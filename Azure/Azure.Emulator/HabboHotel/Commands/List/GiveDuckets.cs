﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class GiveDuckets. This class cannot be inherited.
    /// </summary>
    internal sealed class GiveDuckets : Command
    {
        public GiveDuckets()
        {
            MinParams = 2;
            Description = "Give user duckets.";
            Usage = "[username] [duckets]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_user_can_give_currency");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null)
            {
                session.SendNotif(TextManager.GetText("user_not_found"));
                return;
            }
            int amount;
            if (!int.TryParse(pms[1], out amount))
            {
                session.SendNotif(TextManager.GetText("enter_numbers"));
                return;
            }
            client.GetHabbo().ActivityPoints += amount;
            client.GetHabbo().UpdateActivityPointsBalance();
            client.SendNotif(string.Format(TextManager.GetText("staff_gives_duckets"), session.GetHabbo().UserName, amount));
        }
    }
}