﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HabNam. This class cannot be inherited.
    /// </summary>
    internal sealed class HabNam : Command
    {

        public HabNam()
        {
            MinParams = 0;
            Description = "HabNam.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().VIP || session.GetHabbo().HasFuse("user_is_staff");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;

            var user = room.GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            session.GetHabbo()
                .GetAvatarEffectsInventoryComponent()
                .ActivateCustomEffect(user != null && user.CurrentEffect != 140 ? 140 : 0);

        }
    }
}