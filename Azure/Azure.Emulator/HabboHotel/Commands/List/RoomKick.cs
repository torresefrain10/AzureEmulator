﻿#region

using System;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms.RoomInvokedItems;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RoomKickUsers. This class cannot be inherited.
    /// </summary>
    internal sealed class RoomKickUsers : Command
    {
        public RoomKickUsers()
        {
            MinParams = -1;
            Description = "Kick room.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;

            var alert = string.Join(" ", pms);
            var kick = new RoomKick(alert, (int)session.GetHabbo().Rank);
            AzureEmulator.GetGame()
                .GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, string.Empty,
                    "Room kick", "Kicked the whole room");
            room.QueueRoomKick(kick);
        }
    }
}