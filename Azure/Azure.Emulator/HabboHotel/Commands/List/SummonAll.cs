﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SummonAll. This class cannot be inherited.
    /// </summary>
    internal sealed class SummonAll : Command
    {
        public SummonAll()
        {
            MinParams = 0;
            Description = "Summon all users to room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var reason = string.Join(" ", pms);

            var messageBytes = GameClient.GetBytesNotif(string.Format("You have all been summoned by\r- {0}:\r\n{1}", session.GetHabbo().UserName, reason));
            foreach (GameClient client in AzureEmulator.GetGame().GetClientManager().Clients.Values)
            {
                if (session.GetHabbo().CurrentRoom == null ||
                    session.GetHabbo().CurrentRoomId == client.GetHabbo().CurrentRoomId)
                    continue;

                client.GetMessageHandler().PrepareRoomForUser(session.GetHabbo().CurrentRoom.RoomId, session.GetHabbo().CurrentRoom.RoomData.PassWord);
                client.SendMessage(messageBytes);
            }
        }
    }
}