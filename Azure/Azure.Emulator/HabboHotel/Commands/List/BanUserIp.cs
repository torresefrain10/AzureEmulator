﻿#region

using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class BanUser. This class cannot be inherited.
    /// </summary>
    internal sealed class BanUserIp : Command
    {
        public BanUserIp()
        {
            MinParams = -1;
            Description = "Ban user by IP.";
            Usage = "[ip] [reason]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var user = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);

            if (user == null)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            if (user.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_is_higher_rank"));
                return;
            }
            try
            {
                AzureEmulator.GetGame()
                    .GetBanManager()
                    .BanUser(user, session.GetHabbo().UserName, 788922000.0, string.Join(" ", pms.Skip(1)),
                        true, false);
            }
            catch
            {
                Writer.Writer.LogException("Error while banning");
            }
        }
    }
}