﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshNavigator. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshNavigator : Command
    {
        public RefreshNavigator()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_navigator_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                AzureEmulator.GetGame().GetNavigator().Initialize(adapter);
                AzureEmulator.GetGame().GetRoomManager().LoadModels(adapter);
            }
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}