﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RoomBadge. This class cannot be inherited.
    /// </summary>
    internal sealed class RoomBadge : Command
    {
        public RoomBadge()
        {
            MinParams = 1;
            Description = "Room badge.";
            Usage = "[badge]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            if (pms[0].Length < 2) return;
            var room = session.GetHabbo().CurrentRoom;
            foreach (var current in room.GetRoomUserManager().UserList.Values)
            {
                try
                {
                    if (!current.IsBot && current.GetClient() != null &&
                        current.GetClient().GetHabbo() != null)
                    {
                        current.GetClient()
                            .GetHabbo()
                            .GetBadgeComponent()
                            .GiveBadge(pms[0], true, current.GetClient(), false);
                    }
                }
                catch
                {
                }
            }
            AzureEmulator.GetGame().GetModerationTool()
                .LogStaffEntry(session.GetHabbo().UserName,
                    string.Empty, "Badge",
                    string.Concat("Roombadge in room [", room.RoomId, "] with badge [", pms[0], "]"));
    }
    }
}