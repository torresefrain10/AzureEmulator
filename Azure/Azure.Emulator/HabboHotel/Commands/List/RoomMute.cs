﻿#region

using System;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RoomMute. This class cannot be inherited.
    /// </summary>
    internal sealed class RoomMute : Command
    {
        public RoomMute()
        {
            MinParams = -1;
            Description = "Mute room.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            if (room.RoomMuted)
            {
                session.SendWhisper("Room is already muted.");
                return;
            }
            session.GetHabbo().CurrentRoom.RoomMuted = true;
            var message = new ServerMessage(LibraryParser.OutgoingRequest("AlertNotificationMessageComposer"));
            message.AppendString(string.Format("The room was muted due to:\r{0}", string.Join(" ", pms)));
            message.AppendString(string.Empty);
            room.SendMessage(message);

            AzureEmulator.GetGame()
                .GetModerationTool().LogStaffEntry(session.GetHabbo().UserName, string.Empty,
                    "Room Mute", "Room muted");
        }
    }
}