﻿#region

using System;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SetVideos. This class cannot be inherited.
    /// </summary>
    internal sealed class SetVideos : Command
    {
        internal SetVideos()
        {
            Description = "Setup a video.";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            session.GetHabbo().GetYoutubeManager().RefreshVideos();
        }
    }
}