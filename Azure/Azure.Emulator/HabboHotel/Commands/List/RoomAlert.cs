﻿#region

using System;
using System.Linq;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    internal sealed class RoomAlert : Command
    {
        public RoomAlert()
        {
            MinParams = -1;
            Description = "Send an alert to the whole room.";
            Usage = "[message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("moderator");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var alert = string.Join(" ", pms);
            foreach (
                var user in
                    session.GetHabbo()
                        .CurrentRoom.GetRoomUserManager()
                        .GetRoomUsers()
                        .Where(user => !user.IsBot && user.GetClient() != null))
                user.GetClient().SendNotif(alert);
        }
    }
}