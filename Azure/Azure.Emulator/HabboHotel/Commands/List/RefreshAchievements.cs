﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshAchievements. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshAchievements : Command
    {
        public RefreshAchievements()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_achievements_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            AzureEmulator.GetGame().GetAchievementManager().LoadAchievements(AzureEmulator.GetDatabaseManager().GetQueryReactor());
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}