﻿#region

using System;
using System.Linq;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    internal sealed class MakeSay : Command
    {
        public MakeSay()
        {
            MinParams = -1;
            Description = "Make say user.";
            Usage = "[username] [message]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {

            var room = AzureEmulator.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
            if (room == null) return;

            var user = room.GetRoomUserManager().GetRoomUserByHabbo(pms[0]);
            if (user == null) return;

            var msg = string.Join(" ", pms.Skip(1));
            if (msg.StartsWith(":")) msg = ' ' + msg;

            if (string.IsNullOrEmpty(msg)) return;

            user.Chat(user.GetClient(), msg, false, 0, 0);
                return;

        }
    }
}