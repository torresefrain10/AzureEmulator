﻿#region

using System.Linq;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using System.Text;
using System.Collections.Generic;
using System;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class CommandList. This class cannot be inherited.
    /// </summary>
    internal sealed class CommandList : Command
    {
        public CommandList()
        {
            MinParams = -2;
            Description = "Shows this dialog.";
        }

        public override bool CanExecute(GameClient client)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            string command = "-------------------------------- Commands --------------------------------\r\r";
            
            foreach (KeyValuePair<string, Command> kv in CommandsManager.CommandsDictionary.Where(kv => kv.Value.CanExecute(session)))
            {
                if (string.IsNullOrEmpty(kv.Value.Usage))
                {
                    command += string.Format(":{0} - {1}\r\r", kv.Key, kv.Value.Description);
                }
                else
                {
                    command += string.Format(":{0} {1} - {2}\r\r", kv.Key, kv.Value.Usage, kv.Value.Description);
                }
            }
            
            session.SendNotifWithScroll(command);
        }
    }
}