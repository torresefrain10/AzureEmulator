﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class EmptyUser. This class cannot be inherited.
    /// </summary>
    internal sealed class EmptyUser : Command
    {
        public EmptyUser()
        {
            MinParams = -1;
            Description = "Delete furniture of user.";
            Usage = "[username]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("owner");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var client = AzureEmulator.GetGame().GetClientManager().GetClientByUserName(pms[0]);
            if (client == null || client.GetHabbo().Rank >= session.GetHabbo().Rank)
            {
                session.SendWhisper(TextManager.GetText("user_not_found"));
                return;
            }
            client.GetHabbo().GetInventoryComponent().ClearItems();
        }
    }
}