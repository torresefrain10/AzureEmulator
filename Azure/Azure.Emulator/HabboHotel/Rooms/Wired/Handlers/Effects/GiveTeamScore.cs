﻿#region

using System;
using System.Collections.Generic;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms.Games;

#endregion

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class GiveTeamScore : IWiredItem
    {
        //private List<InteractionType> mBanned;
        public GiveTeamScore(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            OtherString = "10,1,1";
            OtherExtraString = "0";
            OtherExtraString2 = string.Empty;
            //mBanned = new List<InteractionType>();
        }

        public Interaction Type
        {
            get { return Interaction.ActionGiveScore; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; set; }

        public List<RoomItem> Items
        {
            get { return new List<RoomItem>(); }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public string OtherString { get; set; }

        public string OtherExtraString { get; set; }

        public string OtherExtraString2 { get; set; }

        public bool OtherBool { get; set; }

        public bool Execute(params object[] stuff)
        {
            if (stuff[0] == null) return false;
            if ((Interaction)stuff[1] == Interaction.TriggerScoreAchieved) return false;

            var roomUser = (RoomUser)stuff[0];

            if (roomUser == null)
                return false;

            int timesDone;
            int.TryParse(OtherExtraString, out timesDone);

            var scoreToAchieve = 10;
            var maxTimes = 1;
            var _team = 0;

            if (!String.IsNullOrWhiteSpace(OtherString))
            {
                var integers = OtherString.Split(',');
                scoreToAchieve = int.Parse(integers[0]);
                maxTimes = int.Parse(integers[1]);
                _team = int.Parse(integers[2]);
            }

            if (timesDone >= maxTimes || _team == 0)
                return false;

            var team = Team.none;
            switch (_team)
            {
                case 1:
                    team = Team.red;
                    break;
                case 2:
                    team = Team.green;
                    break;
                case 3:
                    team = Team.blue;
                    break;
                case 4:
                    team = Team.yellow;
                    break;
            }
            Room.GetGameManager().AddPointToTeam(team, scoreToAchieve, roomUser);
            timesDone++;

            OtherExtraString = timesDone.ToString();
            return true;
        }
    }
}