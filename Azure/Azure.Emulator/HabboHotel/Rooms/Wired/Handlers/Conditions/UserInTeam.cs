﻿#region

using System.Collections.Generic;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms.Games;

#endregion

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    internal class UserInTeam : IWiredItem
    {
        private int _mDelay;
        public UserInTeam(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            _mDelay = 0;
            Items = new List<RoomItem>();
        }

        public Interaction Type
        {
            get { return Interaction.ConditionUserInTeam; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get
            {
                return _mDelay;
            }
            set
            {
                _mDelay = value;
            }
        }

        public bool Execute(params object[] stuff)
        {
            if (stuff[0] == null) return false;
            RoomUser roomUser = (RoomUser)stuff[0];
            Team team = Team.none;
            int _team = _mDelay / 500;
            switch (_team)
            {
                case 1:
                    team = Team.red;
                    break;
                case 2:
                    team = Team.green;
                    break;
                case 3:
                    team = Team.blue;
                    break;
                case 4:
                    team = Team.yellow;
                    break;
            }
            return roomUser.Team == team;
        }
    }
}