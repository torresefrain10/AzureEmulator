﻿#region

using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;

#endregion

namespace Azure.Configuration
{
    /// <summary>
    /// Struct FurniData
    /// </summary>
    public struct FurniData
    {
        /// <summary>
        /// The identifier
        /// </summary>
        internal int Id;

        /// <summary>
        /// The name
        /// </summary>
        internal string Name;

        /// <summary>
        /// The x
        /// </summary>
        internal ushort X, Y;

        /// <summary>
        /// The can sit
        /// </summary>
        internal bool CanSit, CanWalk;

        /// <summary>
        /// Initializes a new instance of the <see cref="FurniData"/> struct.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="canSit">if set to <c>true</c> [can sit].</param>
        /// <param name="canWalk">if set to <c>true</c> [can walk].</param>
        public FurniData(int id, string name, ushort x = 0, ushort y = 0, bool canSit = false, bool canWalk = false)
        {
            Id = id;
            Name = name;
            X = x;
            Y = y;
            CanSit = canSit;
            CanWalk = canWalk;
        }
    }

    /// <summary>
    /// Class FurniDataParser.
    /// </summary>
    internal static class FurniDataParser
    {
        /// <summary>
        /// The floor items
        /// </summary>
        internal static Dictionary<string, FurniData> FloorItems;

        /// <summary>
        /// The wall items
        /// </summary>
        internal static Dictionary<string, FurniData> WallItems;

        /// <summary>
        /// The _XML parser
        /// </summary>
        private static XmlDocument _xmlParser;

        /// <summary>
        /// The _web client
        /// </summary>
        private static WebClient _webClient;

        /// <summary>
        /// Sets the cache.
        /// </summary>
        public static void SetCache()
        {
            _xmlParser = new XmlDocument();
            _webClient = new WebClient();

            FloorItems = new Dictionary<string, FurniData>();
            WallItems = new Dictionary<string, FurniData>();

            try
            {
                _xmlParser.LoadXml(_webClient.DownloadString(ExtraSettings.FurniDataUrl));
                
                foreach (XmlNode node in _xmlParser.DocumentElement.SelectNodes("/furnidata/roomitemtypes/furnitype"))
                {
                    try
                    {
                        FloorItems.Add(node.Attributes["classname"].Value, new FurniData(int.Parse(node.Attributes["id"].Value), node.SelectSingleNode("name").InnerText, ushort.Parse(node.SelectSingleNode("xdim").InnerText), ushort.Parse(node.SelectSingleNode("ydim").InnerText), node.SelectSingleNode("cansiton").InnerText == "1", node.SelectSingleNode("canstandon").InnerText == "1"));
                    }
                    catch (Exception e)
                    {
                        string valuename = node.Attributes["classname"].Value;
                        if (!string.IsNullOrEmpty(valuename))
                            Console.WriteLine(@"Errror parsing furnidata by {0} with exception: {1}", valuename, e.StackTrace);
                    }
                }

                foreach (XmlNode node in _xmlParser.DocumentElement.SelectNodes("/furnidata/wallitemtypes/furnitype"))
                    WallItems.Add(node.Attributes["classname"].Value, new FurniData(int.Parse(node.Attributes["id"].Value), node.SelectSingleNode("name").InnerText));
            }
            catch (WebException e)
            {
                Console.WriteLine($"Error downloading furnidata.xml: {Environment.NewLine + e}", "Azure.FurniData", ConsoleColor.Red);
                Console.WriteLine("Type a key to close");

                Console.ReadKey();
                Environment.Exit(e.HResult);
            }
            catch (XmlException e)
            {
                Console.WriteLine($"Error parsing furnidata.xml: {Environment.NewLine + e}", "Azure.FurniData", ConsoleColor.Red);
                Console.WriteLine("Type a key to close");

                Console.ReadKey();
                Environment.Exit(e.HResult);
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine($"Error parsing value null of furnidata.xml: {Environment.NewLine + e}", "Azure.FurniData", ConsoleColor.Red);
                Console.WriteLine("Type a key to close");

                Console.ReadKey();
                Environment.Exit(e.HResult);
            }

            _webClient.Dispose();
            _xmlParser = null;
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public static void Clear()
        {
            _webClient.Dispose();
            _xmlParser = null;

            FloorItems.Clear();
            WallItems.Clear();

            FloorItems = null;
            WallItems = null;
        }
    }
}