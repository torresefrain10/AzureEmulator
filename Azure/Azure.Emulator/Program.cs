#region

using System;
using System.Security.Permissions;
using Azure.Configuration;

#endregion

namespace Azure
{
    internal class Program
    {
        /// <summary>
        /// Main Void of Azure.Emulator
        /// </summary>
        /// <param name="args">The arguments.</param>
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        [STAThread]

        public static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;

            Console.Clear();
            InitEnvironment();

            while (AzureEmulator.IsLive)
            {
                Console.CursorVisible = true;
                ConsoleCommandHandling.InvokeCommand(Console.ReadLine());
            }
        }

        /// <summary>
        /// Initialize the Azure Environment
        /// </summary>
        [MTAThread]
        public static void InitEnvironment()
        {
            if (AzureEmulator.IsLive)
                return;

            Console.CursorVisible = false;
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += MyHandler;
            AzureScreen();
            AzureEmulator.Initialize();
        }

        private static void AzureScreen()
        {
            Console.Clear();
            Console.SetWindowSize(Console.LargestWindowWidth > 149 ? 150 : Console.WindowWidth, Console.LargestWindowHeight > 49 ? 50 : Console.WindowHeight);
            Console.SetCursorPosition(0, 0);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine();
            Console.WriteLine(@"     " + @"                                            |         |              ");
            Console.WriteLine(@"     " + @",---.,---,.   .,---.,---.    ,---.,-.-..   .|    ,---.|--- ,---.,---.");
            Console.WriteLine(@"     " + @",---| .-' |   ||    |---'    |---'| | ||   ||    ,---||    |   ||    ");
            Console.WriteLine(@"     " + @"`---^'---'`---'`    `---'    `---'` ' '`---'`---'`---^`---'`---'`    ");
            Console.WriteLine();
            Console.WriteLine(@"     " + @"  BUILD " + AzureEmulator.Version + "." + AzureEmulator.Build + " RELEASE 63B CRYPTO BOTH SIDE");
            Console.WriteLine(@"     " + @"  .NET Framework 4.6     C# 6 Roslyn");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Black;

            Console.WriteLine(
                Console.LargestWindowWidth > 149
                ? @"---------------------------------------------------------------------------------------------------------------------------------------------------"
                : @"-------------------------------------------------------------------------");
        }

        /// <summary>
        /// Mies the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        private static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Logging.DisablePrimaryWriting(true);
            var ex = (Exception)args.ExceptionObject;
            Logging.LogCriticalException(string.Format("SYSTEM CRITICAL EXCEPTION: {0}", ex));
        }
    }
}