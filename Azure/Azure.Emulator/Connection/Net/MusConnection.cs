#region

using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Azure.HabboHotel.GameClients;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;
using System.Data;

#endregion

namespace Azure.Connection.Net
{
    internal class MusConnection
    {
        private Socket Conn;
        private byte[] dataBuffering = new byte[1024]; // 1024

        internal MusConnection(Socket Conn)
        {
            this.Conn = Conn;
            Conn.BeginReceive(dataBuffering, 0, dataBuffering.Length, SocketFlags.None, RecieveData, null);
        }

        internal void RecieveData(IAsyncResult iAr)
        {
            try
            {
                int bytes = 0;
                try
                {
                    bytes = Conn.EndReceive(iAr);
                }
                catch { mDisconnect(); return; }

                String data = Encoding.Default.GetString(dataBuffering, 0, bytes);

                if (data.Length > 0)
                    dArrival(data);
            }
            catch { }
            mDisconnect();
        }

        private void dArrival(string Data)
        {
            try
            {
                var Params = Data.Split(Convert.ToChar(1));
                String header = AzureEmulator.FilterInjectionChars(Params[0]);
                String param = AzureEmulator.FilterInjectionChars(Params[1]);

                GameClient clientByUserId;
                uint userId;
                switch (header)
                {
                    case "sa":
                        string Message = param;

                        ServerMessage serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleChatMessageComposer"));
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendString("Server: " + Message);
                        serverMessage.AppendInteger(0);
                        AzureEmulator.GetGame().GetClientManager().SendStaffMessageRCON(serverMessage);
                        break;

                    case "ha":
                            ServerMessage HotelAlert = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
                            HotelAlert.AppendString(string.Format("{0}\r\n- {1}", param, "Hotel Management"));
                            AzureEmulator.GetGame().GetClientManager().QueueBroadcaseMessage(HotelAlert);
                            break;

                    case "alert":
                        string pUserId = param.Split(new char[] { ' ' })[0];
                        string pMessage = param.Split(new char[] { ' ' })[1];

                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(uint.Parse(pUserId));
                        if (clientByUserId == null) return;
                        clientByUserId.SendNotif(pMessage);
                        break;

                    case "kill":
                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(uint.Parse(param));
                        if (clientByUserId != null)
                            clientByUserId.Disconnect("MUS Disconnection");
                        break;

                    case "updatediamonds":
                        uint UserId = uint.Parse(param);
                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(UserId);

                        if (clientByUserId == null) return;

                        int diamonds;
                        using (IQueryAdapter dbClient = AzureEmulator.GetDatabaseManager().GetQueryReactor())
                        {
                            DataRow row;
                            dbClient.SetQuery("SELECT diamonds FROM users WHERE id = " + UserId);
                            row = dbClient.GetRow();
                            if (row == null) return;

                            diamonds = Convert.ToInt32(row["diamonds"]);
                        }
                        clientByUserId.GetHabbo().Diamonds = diamonds;
                        clientByUserId.GetHabbo().UpdateActivityPointsBalance();
                        break;

                    case "updatemotto":
                        uint UserID = uint.Parse(param);
                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(UserID);

                        if (clientByUserId == null) return;

                        string motto;
                        using (IQueryAdapter conn = AzureEmulator.GetDatabaseManager().GetQueryReactor())
                        {
                            conn.SetQuery("SELECT motto FROM users WHERE id = " + UserID);
                            motto = conn.GetString();
                        }

                        clientByUserId.GetHabbo().Motto = motto;
                        if (clientByUserId.GetHabbo().InRoom)
                        {
                            Room room = clientByUserId.GetHabbo().CurrentRoom;
                            if (room == null) return;

                            RoomUser user = room.GetRoomUserManager().GetRoomUserByHabbo(clientByUserId.GetHabbo().Id);
                            if (user == null) return;

                            ServerMessage message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                            message.AppendInteger(user.VirtualId);
                            message.AppendString(clientByUserId.GetHabbo().Look);
                            message.AppendString(clientByUserId.GetHabbo().Gender.ToLower());
                            message.AppendString(motto);
                            message.AppendInteger(clientByUserId.GetHabbo().AchievementPoints);
                            clientByUserId.SendMessage(message);
                        }
                        break;

                    case "addtoinventory":
                        userId = Convert.ToUInt32(param[0]);
                        var furniId = Convert.ToInt32(param[1]);

                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(userId);
                        if (clientByUserId == null || clientByUserId.GetHabbo() == null ||
                            clientByUserId.GetHabbo().GetInventoryComponent() == null)
                            return;

                        clientByUserId.GetHabbo().GetInventoryComponent().UpdateItems(true);
                        clientByUserId.GetHabbo().GetInventoryComponent().SendNewItems((uint)furniId);

                        break;

                    case "updatecredits":
                        userId = Convert.ToUInt32(param[0]);
                        var credits = Convert.ToInt32(param[1]);

                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(userId);
                        if (clientByUserId != null && clientByUserId.GetHabbo() != null)
                        {
                            clientByUserId.GetHabbo().Credits = credits;
                            clientByUserId.GetHabbo().UpdateCreditsBalance();
                        }
                        return;

                    case "updatesubscription":
                        userId = Convert.ToUInt32(param[0]);

                        clientByUserId = AzureEmulator.GetGame().GetClientManager().GetClientByUserId(userId);
                        if (clientByUserId == null || clientByUserId.GetHabbo() == null) return;
                        clientByUserId.GetHabbo().GetSubscriptionManager().ReloadSubscription();
                        clientByUserId.GetHabbo().SerializeClub();
                        break;

                    case "update_bans":
                        using (IQueryAdapter dbClient = AzureEmulator.GetDatabaseManager().GetQueryReactor())
                            AzureEmulator.GetGame().GetBanManager().LoadBans(dbClient);
                        AzureEmulator.GetGame().GetClientManager().CheckForBanConflicts();
                        break;
                }

            }
            catch { }
            finally { mDisconnect(); }
        }

        private void mDisconnect()
        {
            try { Conn.Close(); }
            catch { }
        }
    }
}