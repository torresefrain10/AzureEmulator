#region

using System;
using System.Globalization;
using System.IO;
using System.Text;

#endregion

namespace Azure.Messages
{
    /// <summary>
    /// Class ClientMessage.
    /// </summary>
    public class ClientMessage : IDisposable
    {
        /// <summary>
        /// The message.
        /// </summary>
        private MemoryStream _message;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientMessage"/> class.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="body">The body.</param>
        internal ClientMessage(int messageId, byte[] body)
        {
            Init(messageId, body);
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        internal int Id { get; private set; }

        /// <summary>
        /// Gets the length of the remaining.
        /// </summary>
        /// <value>The length of the remaining.</value>
        internal int RemainingLength
        {
            get { return (int)(_message.Length - _message.Position); }
        }

        internal int Length
        {
            get { return (int)_message.Length; }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _message.Dispose();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            string stringValue = string.Empty;

            stringValue += Encoding.Default.GetString(_message.ToArray());

            for (int i = 0; i < 13; i++)
                stringValue = stringValue.Replace(char.ToString((char)(i)), string.Format("[{0}]", i));

            return stringValue;
        }

        /// <summary>
        /// Initializes the specified message identifier.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="body">The body.</param>
        internal void Init(int messageId, byte[] body)
        {
            if (body == null)
            {
                body = new byte[0];
            }

            if (_message != null)
            {
                _message.Dispose();
            }

            Id = messageId;
            _message = new MemoryStream(body);
        }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <param name="len">The bytes length.</param>
        /// <returns>System.Byte[].</returns>
        internal byte[] ReadBytes(int len)
        {
            byte[] bytes = new byte[len];
            _message.Read(bytes, 0, len);
            return bytes;
        }

        /// <summary>
        /// Gets the bytes.
        /// </summary>
        /// <param name="len">The bytes length.</param>
        /// <returns>System.Byte[].</returns>
        internal byte[] GetBytes(int len)
        {
            byte[] bytes = ReadBytes(len);
            _message.Position -= len;
            return bytes;
        }

        /// <summary>
        /// Gets the next.
        /// </summary>
        /// <returns>System.Byte[].</returns>
        internal byte[] GetNext()
        {
            int length = HabboEncoding.DecodeInt16(ReadBytes(2));

            return ReadBytes(length);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <returns>System.String.</returns>
        internal string GetString()
        {
            return GetString(Encoding.UTF8);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="encoding">The encoding.</param>
        /// <returns>System.String.</returns>
        internal string GetString(Encoding encoding)
        {
            return encoding.GetString(GetNext());
        }

        /// <summary>
        /// Gets the integer from string.
        /// </summary>
        /// <returns>System.Int32.</returns>
        internal int GetIntegerFromString()
        {
            int result;

            string stringValue = GetString(Encoding.ASCII);

            int.TryParse(stringValue, out result);

            return result;
        }

        /// <summary>
        /// Gets the bool.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal bool GetBool()
        {
            if (RemainingLength <= 0)
                return false;

            return _message.ReadByte() == 1;
        }

        /// <summary>
        /// Gets the integer.
        /// </summary>
        /// <returns>System.Int32.</returns>
        internal int GetInteger()
        {
            if (RemainingLength < 1)
                return 0;

            byte[] bytes = ReadBytes(4);
            return HabboEncoding.DecodeInt32(bytes);
        }

        internal uint PopWiredUInt()
        {
            return uint.Parse(this.GetInteger().ToString());
        }

        internal bool GetIntegerAsBool()
        {
            if (RemainingLength < 1)
                return false;

            return GetInteger() == 1;
        }

        /// <summary>
        /// Gets the integer32.
        /// </summary>
        /// <returns>System.UInt32.</returns>
        internal uint GetUInteger()
        {
            return (uint)(GetInteger());
        }

        /// <summary>
        /// Gets the integer16.
        /// </summary>
        /// <returns>System.UInt16.</returns>
        internal ushort GetUInteger16()
        {
            return (UInt16)(GetInteger());
        }
    }
}