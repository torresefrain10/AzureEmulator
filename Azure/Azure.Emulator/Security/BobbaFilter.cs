﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using Azure.HabboHotel.GameClients;
using Azure.Configuration;
using Azure.Messages.Parsers;
using Azure.Messages;

#endregion

namespace Azure.Security
{
    class BobbaFilter
    {
        internal static List<string> Word;

        internal static bool canTalk(GameClient Session, string Message)
        {
            if (CheckForBannedPhrases(Message) && !Session.GetHabbo().HasFuse("moderator"))
            {
                if (!AzureEmulator.MutedUsersByFilter.ContainsKey(Session.GetHabbo().Id))
                    Session.GetHabbo().BobbaFiltered++;

                if (Session.GetHabbo().BobbaFiltered < 3)
                {
                    Session.SendNotif(TextManager.GetText("wordfilter_inappropriate"));
                }
                else if (Session.GetHabbo().BobbaFiltered == 3)
                {
                    Session.GetHabbo().BobbaFiltered = 4;
                    AzureEmulator.MutedUsersByFilter.Add(Session.GetHabbo().Id, uint.Parse((AzureEmulator.GetUnixTimeStamp() + (300)).ToString()));
                    return false;
                }
                else if (Session.GetHabbo().BobbaFiltered == 5)
                {
                    Session.SendNotif(TextManager.GetText("wordfilter_last_warning"));
                }
                else if (Session.GetHabbo().BobbaFiltered >= 6)
                {
                    Session.GetHabbo().BobbaFiltered = 0;

                    int Length = 3600;
                    AzureEmulator.GetGame().GetBanManager().BanUser(Session, "Auto-system-ban", Length, "ban.", false, false);
                }
            }

            if (AzureEmulator.MutedUsersByFilter.ContainsKey(Session.GetHabbo().Id))
            {
                foreach (KeyValuePair<uint, uint> tim in AzureEmulator.MutedUsersByFilter)
                {
                    int time = int.Parse((AzureEmulator.GetUnixTimeStamp()).ToString());
                    if (tim.Value < time && AzureEmulator.MutedUsersByFilter.ContainsKey(Session.GetHabbo().Id))
                    {
                        AzureEmulator.MutedUsersByFilter.Remove(Session.GetHabbo().Id);
                        return true;
                    }
                    else
                    {
                        Session.SendNotifWithPicture("<br><br><font color=\"#000000\" size=\"13\"> Je hebt een mute tot: <b>" + AzureEmulator.UnixToDateTime(tim.Value) + "</b>", "Spreekverbod", "frank_19", "", "");
                        return true;
                    }
                }
            }
            return true;
        }

        internal static void InitSwearWord()
        {
            Word = new List<string>();
            Word.Clear();
            using (var adapter = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                adapter.SetQuery("SELECT `word` FROM wordfilter");
                var table = adapter.GetTable();
                if (table == null) return;

                foreach (DataRow row in table.Rows)
                {
                    Word.Add(row[0].ToString().ToLower());
                }
            }

            Out.WriteLine("Loaded " + Word.Count + " Bobba Filters", "Azure.Security.BobbaFilter");
            Console.WriteLine();

        }


        internal static bool CheckForBannedPhrases(string Message)
        {
            Message = Message.ToLower();

            Message = Message.Replace(".", "");
            Message = Message.Replace(" ", "");
            Message = Message.Replace("-", "");
            Message = Message.Replace(",", "");

            foreach (string mWord in Word)
            {
                if (Message.Contains(mWord.ToLower()))
                    return true;
            }

            return false;
        }

        private static string ReplaceEx(string original,
                    string pattern, string replacement)
        {
            int count, position0, position1;
            count = position0 = position1 = 0;
            string upperString = original.ToUpper();
            string upperPattern = pattern.ToUpper();
            int inc = (original.Length / pattern.Length) *
                      (replacement.Length - pattern.Length);
            char[] chars = new char[original.Length + Math.Max(0, inc)];
            while ((position1 = upperString.IndexOf(upperPattern, position0, StringComparison.Ordinal)) != -1)
            {
                for (int i = position0; i < position1; ++i)
                    chars[count++] = original[i];
                for (int i = 0; i < replacement.Length; ++i)
                    chars[count++] = replacement[i];
                position0 = position1 + pattern.Length;
            }
            if (position0 == 0) return original;
            for (int i = position0; i < original.Length; ++i)
                chars[count++] = original[i];
            return new string(chars, 0, count);
        }



    }
}

