﻿using Azure.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Manager.Inheritance
{
    // TODO: ...
    class ClearRoomsCache : ICacheable
    {
        public void Parse()
        {
            if (AzureEmulator.GetGame() == null || AzureEmulator.GetGame().GetRoomManager() == null ||
                AzureEmulator.GetGame().GetRoomManager().LoadedRoomData == null)
                return;

            List<uint> RoomsToRemove = new List<uint>();

            foreach (KeyValuePair<uint, RoomData> Room in AzureEmulator.GetGame().GetRoomManager().LoadedRoomData)
            {
                if (Room.Value.UsersNow <= 0) 
                    continue;

                if ((DateTime.Now - Room.Value.LastUsed).TotalMilliseconds < 1800000)
                    continue;

                RoomsToRemove.Add(Room.Key);
            }

            foreach (uint RoomId in RoomsToRemove)
            {
                RoomData Room;

                if (!AzureEmulator.GetGame().GetRoomManager().LoadedRoomData.TryRemove(RoomId, out Room))
                {
                    Room = null;
                }
            }
        }
    }
}
