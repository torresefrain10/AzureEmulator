﻿using Azure.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Manager.Inheritance
{
    class ClearUserCache : ICacheable
    {
        public void Parse()
        {
            List<uint> HabbosToRemvoe = new List<uint>();

            foreach (KeyValuePair<uint, Habbo> Habbo in AzureEmulator.UsersCached)
            {
                if (Habbo.Value == null)
                {
                    HabbosToRemvoe.Add(Habbo.Key);
                    return;
                }

                if (AzureEmulator.GetGame().GetClientManager().Clients.ContainsKey(Habbo.Key))
                    continue;

                if ((DateTime.Now - Habbo.Value.LastUsed).TotalMilliseconds < 1800000)
                    continue;

                HabbosToRemvoe.Add(Habbo.Key);
            }

            foreach (uint HabboId in HabbosToRemvoe)
            {
                Habbo Habbo;

                if (!AzureEmulator.UsersCached.TryRemove(HabboId, out Habbo))
                {
                    // No need to throw an exception, the user must've logged out before the function finished.
                }
            }
        }
    }
}
